package pages.weare.socialnetwork;

import org.openqa.selenium.WebDriver;

public class HomePage extends BaseSocialNetworkPage {

    public HomePage(WebDriver webDriver) {
        super(webDriver, "home.page");
    }

    public void logoutUser() {

        actions.waitForElementClickable("home.page.homeButton");
        actions.clickElement("home.page.homeButton");

        actions.waitForElementClickable("home.page.logoutButton");
        actions.clickElement("home.page.logoutButton");
        actions.assertElementPresent("login.page.logoutText");
    }
}

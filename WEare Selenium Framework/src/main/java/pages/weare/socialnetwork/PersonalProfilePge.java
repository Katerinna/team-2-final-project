package pages.weare.socialnetwork;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

public class PersonalProfilePge extends BaseSocialNetworkPage {

    private String firstName = "Firstname";
    private String lastName = "Lastname";
    private final String birthDate = "05/07/1989";

    public PersonalProfilePge(WebDriver webDriver) {
        super(webDriver, "home.page");
    }

    private String generateRandomFirstName() {

        String randomStr = RandomStringUtils.randomAlphabetic(5);
        firstName = firstName.concat(randomStr);
        return firstName;
    }

    private String generateRandomLastName() {

        String randomStr = RandomStringUtils.randomAlphabetic(5);
        lastName = lastName.concat(randomStr);
        return lastName;
    }


    public void adminEditOtherUserProfile() {

        navigateToPage();
        assertNavigatedUrl();
        adminZonePart();
        editProfile();
    }

    public void userEditPersonalProfile() {

        actions.waitForElementClickable("home.page.personalProfileButton");
        actions.clickElement("home.page.personalProfileButton");

        editProfile();
    }

    public void userConnectWithOtherUser() {

        navigateToPage();
        assertNavigatedUrl();

        actions.waitForElementClickable("home.page.searchParam2");
        actions.clickElement("home.page.searchParam2");
        actions.typeValueInField(getUserFullName(), "home.page.searchParam2");
        actions.clickElement("home.page.searchButton");

        actions.scrollDown("personal.profile.page.seeProfileButton");
        actions.waitForElementClickable("personal.profile.page.seeProfileButton");
        actions.clickElement("personal.profile.page.seeProfileButton");

        actions.waitForElementClickable("personal.profile.page.connectButton");
        actions.clickElement("personal.profile.page.connectButton");

        actions.waitForElementVisible("personal.profile.page.successfulConnectionMessage");
        actions.assertElementPresent("personal.profile.page.successfulConnectionMessage");
    }

    public void userAcceptFriendRequest() {

        actions.waitForElementClickable("home.page.personalProfileButton");
        actions.clickElement("home.page.personalProfileButton");

        actions.waitForElementClickable("personal.profile.page.newRequestButton");
        actions.clickElement("personal.profile.page.newRequestButton");

        actions.waitForElementClickable("personal.profile.page.acceptFriedRequestButton");
        actions.clickElement("personal.profile.page.acceptFriedRequestButton");

        actions.waitForElementVisible("personal.profile.page.noRequestMessage");
        actions.assertElementPresent("personal.profile.page.noRequestMessage");
    }

    public void editProfile() {

        actions.waitForElementVisible("personal.profile.page.editProfileButton");
        actions.clickElement("personal.profile.page.editProfileButton");

        actions.scrollDown("personal.profile.page.updateProfileButton");
        actions.waitForElementVisible("personal.profile.page.firstName");
        actions.typeValueInField(generateRandomFirstName(), "personal.profile.page.firstName");

        actions.clickElement("personal.profile.page.lastName");
        actions.typeValueInField(generateRandomLastName(), "personal.profile.page.lastName");

        actions.clickElement("personal.profile.page.birthDate");
        actions.typeValueInField(birthDate, "personal.profile.page.birthDate");

        actions.waitForElementClickable("personal.profile.page.updateProfileButton");
        actions.clickElement("personal.profile.page.updateProfileButton");

        actions.waitForElementClickable("personal.profile.page.profileButton");
        actions.clickElement("personal.profile.page.profileButton");

        actions.waitForElementVisible("personal.profile.page.viewName");
        actions.assertElementPresent("personal.profile.page.viewName");
    }


    public void adminDisableUser() {

        navigateToPage();
        adminZonePart();

        if (actions.isElementVisible("personal.profile.page.disableUserButton")) {
            actions.clickElement("personal.profile.page.disableUserButton");
        } else {
            actions.clickElement("personal.profile.page.enableUserButton");
            actions.waitForElementClickable("personal.profile.page.disableUserButton");
            actions.clickElement("personal.profile.page.disableUserButton");
        }

        actions.waitForElementVisible("personal.profile.page.enableUserButton");
        actions.assertElementPresent("personal.profile.page.enableUserButton");

    }

    public void adminEnableUser() {

        navigateToPage();
        adminZonePart();

        if (actions.isElementVisible("personal.profile.page.enableUserButton")) {
            actions.clickElement("personal.profile.page.enableUserButton");
        } else {
            actions.clickElement("personal.profile.page.disableUserButton");
            actions.waitForElementClickable("personal.profile.page.enableUserButton");
            actions.clickElement("personal.profile.page.enableUserButton");
        }

        actions.waitForElementVisible("personal.profile.page.disableUserButton");
        actions.assertElementPresent("personal.profile.page.disableUserButton");
    }

    public void adminZonePart() {

        actions.waitForElementClickable("home.page.goTo.admin.zone");
        actions.clickElement("home.page.goTo.admin.zone");

        actions.waitForElementClickable("home.page.admin.zone.viewUsersButton");
        actions.clickElement("home.page.admin.zone.viewUsersButton");

        actions.scrollDown("home.page.admin.zone.selectUser");
        actions.waitForElementVisible("home.page.admin.zone.selectUser");
        actions.clickElement("home.page.admin.zone.selectUser");
    }

    public String getUserFullName() {
        return firstName + " " + lastName;
    }

}

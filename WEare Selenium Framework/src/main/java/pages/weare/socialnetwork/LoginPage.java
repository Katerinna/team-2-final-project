package pages.weare.socialnetwork;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends BaseSocialNetworkPage {

    public LoginPage(WebDriver webDriver) {
        super(webDriver, "login.page");
    }

    public void loginUser(String username) {

        String password = getConfigPropertyByKey("login.page.password");

        actions.waitForElementClickable("login.page.signInButton");
        actions.clickElement("login.page.signInButton");

        actions.waitForElementClickable("login.page.username");
        actions.clickElement("login.page.username");
        actions.typeValueInField(username, "login.page.username");

        actions.waitForElementClickable("login.page.password");
        actions.clickElement("login.page.password");
        actions.typeValueInField(password, "login.page.password");

        actions.waitForElementClickable("login.page.submitButton");
        actions.clickElement("login.page.submitButton");
        actions.assertElementPresent("login.page.logoutButton");
    }

    public void loginAdmin(String username) {

        String password = getConfigPropertyByKey("userAdmin.password");

        actions.waitForElementClickable("login.page.signInButton");
        actions.clickElement("login.page.signInButton");

        actions.waitForElementClickable("login.page.username");
        actions.clickElement("login.page.username");
        actions.typeValueInField(username, "login.page.username");

        actions.waitForElementClickable("login.page.password");
        actions.clickElement("login.page.password");
        actions.typeValueInField(password, "login.page.password");

        actions.waitForElementClickable("login.page.submitButton");
        actions.clickElement("login.page.submitButton");
        actions.assertElementPresent("login.page.logoutButton");
    }

}

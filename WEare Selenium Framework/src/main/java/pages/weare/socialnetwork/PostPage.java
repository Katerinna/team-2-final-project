package pages.weare.socialnetwork;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

public class PostPage extends BaseSocialNetworkPage {

    private String baseText = "This is random generated text ";
    private String baseCommentText = "This is comment ";


    public PostPage(WebDriver webDriver) {
        super(webDriver, "post.page");
    }

    private String generateRandomText() {

        String randomStr = RandomStringUtils.randomAlphabetic(20);
        baseText = baseText.concat(randomStr);
        return baseText;
    }

    public void createNewPost() {

        actions.waitForElementClickable("home.page.addNewPostButton");
        actions.clickElement("home.page.addNewPostButton");

        actions.waitForElementClickable("login.page.addNewPostButton");
        actions.clickElement("login.page.addNewPostButton");

        actions.waitForElementClickable("post.page.postVisibility");
        actions.clickElement("post.page.postVisibility");
        actions.clickElement("post.page.selectPublicPost");

        actions.waitForElementClickable("post.page.messageBox");
        actions.clickElement("post.page.messageBox");
        actions.typeValueInField(generateRandomText(), "post.page.messageBox");
        actions.scrollDown("post.page.saveButton");
        actions.clickElement("post.page.saveButton");
        actions.assertElementPresent(String.format("//p[contains(text(),'%s')]", baseText));
    }

    public void createNewPrivatePost() {

        actions.waitForElementClickable("home.page.addNewPostButton");
        actions.clickElement("home.page.addNewPostButton");

        actions.waitForElementClickable("login.page.addNewPostButton");
        actions.clickElement("login.page.addNewPostButton");

        actions.waitForElementClickable("post.page.postVisibility");
        actions.clickElement("post.page.postVisibility");
        actions.clickElement("post.page.selectPrivatePost");

        actions.waitForElementClickable("post.page.messageBox");
        actions.clickElement("post.page.messageBox");
        actions.typeValueInField(generateRandomText(), "post.page.messageBox");
        actions.scrollDown("post.page.saveButton");
        actions.clickElement("post.page.saveButton");
        actions.assertElementPresent(String.format("//p[contains(text(),'%s')]", baseText));

    }

    public void addComment() {

        actions.waitForElementClickable("home.page.latestPostButton");
        actions.clickElement("home.page.latestPostButton");

        actions.waitForElementClickable("post.page.explorePostButton");
        actions.clickElement("post.page.explorePostButton");

        actions.scrollDown("post.page.messageBox");

        actions.waitForElementClickable("post.page.messageBox");
        actions.clickElement("post.page.messageBox");
        actions.typeValueInField(baseCommentText + generateRandomText(), "post.page.messageBox");

        actions.waitForElementClickable("post.page.postCommentButton");
        actions.clickElement("post.page.postCommentButton");

        actions.scrollDown("post.page.showCommentsButton");
        actions.waitForElementClickable("post.page.showCommentsButton");
        actions.clickElement("post.page.showCommentsButton");
        actions.assertElementPresent(String.format("//p[contains(text(),'%s')]", baseCommentText));


    }

    public void clickLikeButtonOnPublicPost() {

        actions.waitForElementClickable("home.page.latestPostButton");
        actions.clickElement("home.page.latestPostButton");
        actions.waitForElementClickable("post.page.likeButton");
        actions.clickElement("post.page.likeButton");
        actions.waitForElementVisible("post.page.dislikeButton");
        actions.assertElementPresent("post.page.dislikeButton");
    }


    public void adminDeleteOtherUsersPost() {

        actions.waitForElementClickable("home.page.latestPostButton");
        actions.clickElement("home.page.latestPostButton");

        actions.waitForElementClickable("post.page.explorePostButton");
        actions.clickElement("post.page.explorePostButton");

        actions.waitForElementClickable("post.page.deletePostButton");
        actions.clickElement("post.page.deletePostButton");

        actions.scrollDown("post.page.deletedConfirmedButton");
        actions.clickElement("post.page.deletedConfirmedButton");
        actions.clickElement("post.page.selectDeletePost");

        actions.clickElement("post.page.submitButton");

        actions.waitForElementVisible("post.page.post.delete.successfulMessage");
        actions.assertElementPresent("post.page.post.delete.successfulMessage");

    }

    public void adminDeleteOtherUsersComment() {

        actions.waitForElementClickable("home.page.latestPostButton");
        actions.clickElement("home.page.latestPostButton");

        actions.waitForElementClickable("post.page.explorePostButton");
        actions.clickElement("post.page.explorePostButton");

        actions.scrollDown("post.page.showCommentsButton");
        actions.waitForElementClickable("post.page.showCommentsButton");
        actions.clickElement("post.page.showCommentsButton");

        actions.waitForElementClickable("post.page.deleteCommentButton");
        actions.clickElement("post.page.deleteCommentButton");

        actions.scrollDown("post.page.deletedConfirmedButton");
        actions.clickElement("post.page.deletedConfirmedButton");
        actions.clickElement("post.page.selectDeleteComment");

        actions.clickElement("post.page.submitButton");

        actions.waitForElementVisible("post.page.comment.delete.successfulMessage");
        actions.assertElementPresent("post.page.comment.delete.successfulMessage");

    }

}

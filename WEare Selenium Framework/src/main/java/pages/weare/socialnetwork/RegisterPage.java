package pages.weare.socialnetwork;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;


import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class RegisterPage extends BaseSocialNetworkPage {
    private final String userNameBase = "User";
    private final String adminNameBase = "admin";
    private String userName = "User";
    private String adminName = "admin";

    public RegisterPage(WebDriver webDriver) {
        super(webDriver, "home.page");
    }

    private String generateRandomName() {

        String randomStr = RandomStringUtils.randomAlphabetic(7);
        userName = userNameBase.concat(randomStr);
        return userName;
    }

    private String generateRandomNameAdmin() {

        String randomStr = RandomStringUtils.randomAlphabetic(7);
        adminName = adminNameBase.concat(randomStr);
        return adminName;
    }

    public String getUserName() {
        return userName;
    }

    public String getAdminName() {
        return adminName;
    }

    public void registerUser() {

        String password = getConfigPropertyByKey("user.password");
        String email = getConfigPropertyByKey("user.email");

        actions.waitForElementClickable("home.page.homeButton");
        actions.clickElement("home.page.homeButton");
        actions.waitForElementClickable("home.page.registerButton");
        actions.clickElement("home.page.registerButton");

        actions.waitForElementClickable("register.page.name");
        actions.clickElement("register.page.name");
        actions.typeValueInField(generateRandomName(), "register.page.name");

        actions.waitForElementClickable("register.page.email");
        actions.clickElement("register.page.email");
        actions.typeValueInField(email, "register.page.email");

        actions.waitForElementClickable("register.page.password");
        actions.clickElement("register.page.password");
        actions.typeValueInField(password, "register.page.password");

        actions.waitForElementClickable("register.page.confirmPassword");
        actions.clickElement("register.page.confirmPassword");
        actions.typeValueInField(password, "register.page.confirmPassword");

        actions.waitForElementClickable("register.page.submitButton");
        actions.clickElement("register.page.submitButton");

        actions.waitForElementVisible("successfulRegistration");
        actions.assertElementPresent("successfulRegistration");
    }

    public void registerAdmin() {

        String password = getConfigPropertyByKey("userAdmin.password");
        String email = getConfigPropertyByKey("userAdmin.email");

        actions.waitForElementClickable("home.page.homeButton");
        actions.clickElement("home.page.homeButton");

        actions.waitForElementClickable("home.page.registerButton");
        actions.clickElement("home.page.registerButton");

        actions.waitForElementClickable("register.page.name");
        actions.clickElement("register.page.name");
        actions.typeValueInField(generateRandomNameAdmin(), "register.page.name");

        actions.waitForElementClickable("register.page.email");
        actions.clickElement("register.page.email");
        actions.typeValueInField(email, "register.page.email");

        actions.waitForElementClickable("register.page.password");
        actions.clickElement("register.page.password");
        actions.typeValueInField(password, "register.page.password");

        actions.waitForElementClickable("register.page.confirmPassword");
        actions.clickElement("register.page.confirmPassword");
        actions.typeValueInField(password, "register.page.confirmPassword");

        actions.waitForElementClickable("register.page.submitButton");
        actions.clickElement("register.page.submitButton");

        actions.waitForElementVisible("successfulRegistration");
        actions.assertElementPresent("successfulRegistration");
    }
}

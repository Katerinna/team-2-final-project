package test.cases.socialNetwork;

import org.junit.Test;
import pages.weare.socialnetwork.RegisterPage;
import test.cases.BaseTestSetup;

public class RegisterTests extends BaseTestSetup {

    @Test
    public void register() {
        RegisterPage registerPage = new RegisterPage(actions.getDriver());
        registerPage.registerUser();
    }

    @Test
    public void registerAdmin() {
        RegisterPage registerPage = new RegisterPage(actions.getDriver());
        registerPage.registerAdmin();

    }
}

package test.cases.socialNetwork.suites;

import test.cases.socialNetwork.*;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses({RegisterTests.class, PostTests.class, CommentTests.class, ConnectTests.class,
        PersonalProfileTests.class, DisableEnableUserTests.class})

@RunWith(Suite.class)
public class RunSuite {
}


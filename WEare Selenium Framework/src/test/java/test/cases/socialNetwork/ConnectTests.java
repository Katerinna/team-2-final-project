package test.cases.socialNetwork;

import jdk.jfr.Description;
import org.junit.Test;
import pages.weare.socialnetwork.PersonalProfilePge;
import test.cases.BaseTestSetup;

public class ConnectTests extends BaseTestSetup {

    @Test
    public void userWantsToConnectWithOtherUsers(){
        registerNewUser();
        login();
        PersonalProfilePge personalProfilePge = new PersonalProfilePge(actions.getDriver());
        personalProfilePge.userEditPersonalProfile();
        logout();
        registerNewAdmin();
        loginAdmin();
        personalProfilePge.userConnectWithOtherUser();
        logout();
        login();
        personalProfilePge.userAcceptFriendRequest();
        logout();
    }
}

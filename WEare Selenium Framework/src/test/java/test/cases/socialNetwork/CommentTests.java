package test.cases.socialNetwork;

import org.junit.Test;
import pages.weare.socialnetwork.PostPage;
import test.cases.BaseTestSetup;

public class CommentTests extends BaseTestSetup {

    @Test
    public void addCommentToNewlyCreatedPost() {
        registerNewUser();
        login();
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.createNewPost();
        logout();
        registerNewUser();
        login();
        postPage.addComment();
        logout();
    }

    @Test
    public void adminWantsToDeleteOtherUserComment() {
        registerNewUser();
        login();
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.createNewPost();
        postPage.addComment();
        logout();
        registerNewAdmin();
        loginAdmin();
        postPage.adminDeleteOtherUsersComment();
        logout();
    }
}

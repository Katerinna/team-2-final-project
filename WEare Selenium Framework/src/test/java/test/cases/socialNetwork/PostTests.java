package test.cases.socialNetwork;

import org.junit.Test;
import pages.weare.socialnetwork.PostPage;
import test.cases.BaseTestSetup;

public class PostTests extends BaseTestSetup {

    @Test
    public void userWantToCreatePublicPost() {
        registerNewUser();
        login();
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.createNewPost();
        logout();
    }

    @Test
    public void userWantToCreatePrivatePost() {
        registerNewUser();
        login();
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.createNewPrivatePost();
        logout();
    }

    @Test
    public void adminWantsToDeleteOtherUserPost() {
        registerNewUser();
        login();
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.createNewPost();
        logout();
        registerNewAdmin();
        loginAdmin();
        postPage.adminDeleteOtherUsersPost();
        logout();
    }

    @Test
    public void userWantToLikePublicPost() {
        registerNewUser();
        login();
        PostPage postPage = new PostPage(actions.getDriver());
        postPage.createNewPost();
        postPage.clickLikeButtonOnPublicPost();
        logout();
    }

}

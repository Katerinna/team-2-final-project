package test.cases.socialNetwork;

import org.junit.Test;
import pages.weare.socialnetwork.PersonalProfilePge;
import test.cases.BaseTestSetup;

public class DisableEnableUserTests extends BaseTestSetup {

    @Test
    public void adminWantsToDisableUser() {
        registerNewUser();
        registerNewAdmin();
        loginAdmin();
        PersonalProfilePge personalProfilePge = new PersonalProfilePge(actions.getDriver());
        personalProfilePge.adminDisableUser();
        logout();
    }

    @Test
    public void adminWantToEnableUser() {
        registerNewUser();
        registerNewAdmin();
        loginAdmin();
        PersonalProfilePge personalProfilePge = new PersonalProfilePge(actions.getDriver());
        personalProfilePge.adminEnableUser();
        logout();
    }

}

package test.cases.socialNetwork;

import org.junit.Test;
import pages.weare.socialnetwork.PersonalProfilePge;
import test.cases.BaseTestSetup;

public class PersonalProfileTests extends BaseTestSetup {

    @Test
    public void adminWantsToEditOtherUserProfile(){
        registerNewAdmin();
        loginAdmin();
        PersonalProfilePge personalProfilePge = new PersonalProfilePge(actions.getDriver());
        personalProfilePge.adminEditOtherUserProfile();
        logout();
    }

    @Test
    public void userWantsToEditOwnProfile(){
        registerNewUser();
        login();
        PersonalProfilePge personalProfilePge = new PersonalProfilePge(actions.getDriver());
        personalProfilePge.userEditPersonalProfile();
        logout();
    }
}

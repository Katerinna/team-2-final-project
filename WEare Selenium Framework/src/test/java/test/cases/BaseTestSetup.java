package test.cases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.weare.socialnetwork.HomePage;
import pages.weare.socialnetwork.LoginPage;
import pages.weare.socialnetwork.RegisterPage;

public class BaseTestSetup {

    protected UserActions actions = new UserActions();

    RegisterPage registerPage = new RegisterPage(actions.getDriver());

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("home.page");
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }

    public void registerNewUser() {
        registerPage.registerUser();
    }

    public void registerNewAdmin() {
        registerPage.registerAdmin();
    }

    public void login() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser(registerPage.getUserName());
    }

    public void loginAdmin() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginAdmin(registerPage.getAdminName());
    }

    public void logout() {
        HomePage homePage = new HomePage(actions.getDriver());
        homePage.logoutUser();
    }
}

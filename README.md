# Team 2 WEare Social Network - Final Project 

Prerequisites:
Clone the repository https://gitlab.com/ZzlatanIvanov/team-2-final-project

### Postman Tests
### For successfull execution of the tests from the collection please follow the steps below:
1. Dowload and install Postman from https://www.postman.com/downloads/
2. Dowload and install Node.js from https://nodejs.org/en/download/
3. Install Newman by opening cmd and type the following command "npm install -g newman"
4. Install the tool htmlextra reporter by typing in the cmd the following command "npm install -g newman-reporter-htmlextra"

### Instructions for starting the project�s tests:

1. Open the Postman folder
2. Double click on the "script.bat" and wait for the tests to be executed
3. Two CMD windows should appear. Press any key at the one with the counter and the execution will start
4. Results can be seen in the html file located in the folder "results"


### Selenium Tests
### For successfull execution of the tests from the collection please follow the steps below:
    1. Install IntelliJ IDEA Ultimate 
    2. Install JDK 11

### Instructions for starting the project�s tests:
1.	Open config.properties and fill the missing user.password, user.email, userAdmin.password, userAdmin.email, login.page.password (admin email should contans "admin" ).
	You could always contact us in case you want to use ours existing credentials.
2.	All tests could be run by:
	-opening IntelliJ IDEA Ultimate and then click start at RunSuite.class 
	-or right click MavenScript.bat file and choose run MavenScript
3.	After execution of MavenScript.bat file, report will be available in html format in project folder target/site � surefire-report.html
4.	When you double click at the report, browsers icons will appear, choose one for better report visualization


### JIRA 

Test cases and test plan can be seen JIRA management tool:

https://weare-socielnetwork-38.atlassian.net
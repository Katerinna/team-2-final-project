# Team 2 WEare Social Network - Final Project  

###Docker

### For successfull execution of perormance and load tests please follow the steps below:


Prerequisites:

. Clone the repository https://gitlab.com/ZzlatanIvanov/team-2-final-project

. Install Docker

Windows
Mac
Linux
Verify docker is present by starting terminal or cmd and enter docker --version

If old instalation of docker is present - update it.


- Verify Docker Compose is present or install it (this step is needed for Linux OS)

Install Docker Compose (https://docs.docker.com/compose/install/)


- Clone the repository or download the file [docker-compose.yml](./docker-compose.yml)

- Start terminal or cmd in the directory containing the docker-compose.yml file

- Start/stop the application

Run this command to start: docker-compose up

Images will be build/downloaded and start. If the terminal is closed - the app will stop!


Run this command to start: docker-compose up -d

Images will be build/downloaded and started in containers in a detached mode (closing the terminal won't stop the application)


Run this command to stop: docker-compose down



- Verify the app is running

Open http://localhost:8081/ in a browser

###jMter

1. Dowload and install jMeter from https://www.apache.org/
2. Open jMeter and click ''open'' button and browse the file WEare SN.jmx
3. For viewing the result Add some listener (simple data writer) and store the result in csv file 
4. Select scenario and then "click" strat button
5. Using GUI Mode: Firstly, create one folder at any location and in JMeter Go to Tools -> Generate HTML Report
6. Firstly on Result File put the CSV file path where our result is stored
In User.properties file section mentions the location of the user.properties file
In the output, directory put the newly created folder path
7. Now, Click on the Generate Report and our report is generated
8. Now open the folder which we created and we got out HTML report, so simply open the HTML report




